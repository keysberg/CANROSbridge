#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import UInt16MultiArray, Float32MultiArray, UInt8MultiArray, MultiArrayDimension
from amiro_remote.client import *


def main():
    rospy.init_node('CANROSbridge', anonymous=True)

    if not rospy.has_param('~esp_ip'):
        raise Exception('No ip found for connection with ESP.')

    if not rospy.has_param('~topics'):
        raise Exception('No topics found for reading and sending.')

    esp_ip = rospy.get_param('~esp_ip')
    rospy.loginfo("try connecting to ip %s", esp_ip)
    topic_names = [t.strip() for t in rospy.get_param('~topics').strip('[]').split(',')]
    rospy.loginfo("listening to topics: %s", topic_names)
    topic_ids = [topic_name2id[t] for t in topic_names]
    rospy.loginfo("topics translated to ids: %s", topic_ids)

    amr_client = AmiroRemoteClient(esp_ip, 1234, topics=topic_ids)

    # Create list of ROS publishers
    publisher_list = []
    latest_values = []
    # Dictionary of topic ids and corresponding ros message types
    topic_msg_types = {}
    # remove board id from layout dictionary, as it is unknown
    layout_keys = list(zip(*topic_layout.keys()))[1]
    layout_values = list(topic_layout.values())
    for i in range(len(topic_ids)):
        # Find topic in the layout dictionary
        layout_idx = [d for d in range(len(layout_keys)) if layout_keys[d] == topic_ids[i]]
        if len(layout_idx) < 1:
            raise Exception("No layout found for the defined topic {} {}.".format(topic_ids[i], topic_names[i]))
        # As the board is unknown, take first matching layout of topic
        layout_idx = layout_idx[0]
        layout = layout_values[layout_idx]
        if layout[0] == 'H':
            topic_msg_types[topic_ids[i]] = (len(layout), UInt16MultiArray)
        elif layout[0] == 'f':
            topic_msg_types[topic_ids[i]] = (len(layout), Float32MultiArray)
        elif layout[0] == 'B':
            topic_msg_types[topic_ids[i]] = (len(layout), UInt8MultiArray)
        else:
            raise Exception("Unknown layout {} found for topic {} {}".format(layout[0], topic_ids[i], topic_names[i]))
        publisher_list.append(rospy.Publisher(topic_names[i], topic_msg_types[topic_ids[i]][1], queue_size=10))
        latest_values.append(None)

    rate = rospy.Rate(100)
    # Listen for messages and publish them to ROS
    with amr_client:
        while not rospy.is_shutdown():
            for i in range(len(topic_ids)):
                values = amr_client.collect_data(topic_ids[i])
                for v in range(len(values)):
                    message = topic_msg_types[topic_ids[i]][1](data=values[v].values)
                    message.layout.dim = [MultiArrayDimension(label="length", size=topic_msg_types[topic_ids[i]][0],
                                                              stride=topic_msg_types[topic_ids[i]][0])]
                    publisher_list[i].publish(message)
                    # rospy.loginfo("Published for topic %i %s", topic_ids[i], topic_names[i])
                """
                ##############Read only latest values##############
                values = amr_client.latest.get(topic_ids[i])
                if (values is None) or (np.array_equal(values.values, latest_values[i])):
                    # rospy.loginfo("Topic %i %s was empty", topic_ids[i], topic_names[i])
                    None
                else:
                    message = topic_msg_types[topic_ids[i]][1](data=values.values)
                    message.layout.dim = [MultiArrayDimension(label="length", size=topic_msg_types[topic_ids[i]][0],
                                                            stride=topic_msg_types[topic_ids[i]][0])]
                    publisher_list[i].publish(message)
                    # rospy.loginfo("Published for topic %i %s", topic_ids[i], topic_names[i])
                    latest_values[i] = values.values
                """
            rate.sleep()
        amr_client.disconnect()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        print("passing")
        pass



