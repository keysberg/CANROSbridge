#!/usr/bin/env python

import rospy
from std_msgs.msg import UInt16MultiArray, Float32MultiArray, UInt8MultiArray, MultiArrayDimension
from amiro_remote.client import *


def callback_print(data):
    # data_length = data.layout.dim[0].size
    rospy.loginfo("i heard data %s", data.data)


def main():
    rospy.init_node('listener', anonymous=True)

    if not rospy.has_param('~topics'):
        raise Exception('No topics found for reading and sending.')

    topic_names = [t.strip() for t in rospy.get_param('~topics').strip('[]').split(',')]
    rospy.loginfo("listening to topics: %s", topic_names)
    topic_ids = [topic_name2id[t] for t in topic_names]
    rospy.loginfo("topics translated to ids: %s", topic_ids)

    # Create list of ROS subscribers
    subscriber_list = []
    # remove board id from layout dictionary, as it is unknown
    layout_keys = list(zip(*topic_layout.keys()))[1]
    layout_values = list(topic_layout.values())
    for i in range(len(topic_ids)):
        # Find topic in the layout dictionary
        layout_idx = [d for d in range(len(layout_keys)) if layout_keys[d] == topic_ids[i]]
        if len(layout_idx) < 1:
            raise Exception("No layout found for the defined topic {} {}.".format(topic_ids[i], topic_names[i]))
        # As the board is unknown, take first matching layout of topic
        layout_idx = layout_idx[0]
        layout = layout_values[layout_idx]
        if layout[0] == 'H':
            subscriber_list.append(rospy.Subscriber(topic_names[i], UInt16MultiArray, callback_print))
        elif layout[0] == 'f':
            subscriber_list.append(rospy.Subscriber(topic_names[i], Float32MultiArray, callback_print))
        elif layout[0] == 'B':
            subscriber_list.append(rospy.Subscriber(topic_names[i], UInt8MultiArray, callback_print))
        else:
            raise Exception("Unknown layout {} found for topic {} {}".format(layout[0], topic_ids[i], topic_names[i]))

    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass



