#!/usr/bin/env python

import rospy
import math
from std_msgs.msg import UInt16MultiArray, Float32MultiArray, UInt8MultiArray, MultiArrayDimension
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point
from nav_msgs.msg import Odometry

"""
main difference to Display Node is: resets the odometry at junctions
"""
class AdaptiveDisplayNode:

    def pm_callback(self, data):
        # data_length = data.layout.dim[0].size
        # rospy.loginfo("i heard data %s", data.data)
        if (data.data[0] == 0):
            self.old_pos = new_pos = 0
            print("Reset position due to standby.")
        else:
            new_pos = data.data[6]
        if (self.old_pos,new_pos) in self.coord_dict:
            self.x_offset, self.y_offset, self.angle_reset = self.coord_dict[(self.old_pos, new_pos)] 
            print("Arrived at position ({}-{}) with coordinates (X:{},Y:{})".format(self.old_pos,new_pos,self.x_offset,self.y_offset))
        else:
            print("Key ({}-{}) not found :O".format(self.old_pos, new_pos))
        
        self.old_pos = new_pos
        self.print_ctr = 0
        
        # show the latest secured position on map
        self.odom_pos.pose.position.x = self.x_offset
        self.odom_pos.pose.position.y = self.y_offset
        self.odom_pos.scale.z = 0.01
        self.odom_pos.pose.position.z = 0.5 * self.odom_pos.scale.z
        
            
    def odom_callback(self, data):
        # data_length = data.layout.dim[0].size
        # rospy.loginfo("i heard data %s", data.data)
        
        # add position to path
        x,y = self.rotate((self.x_offset, self.y_offset),(data.data[0] + self.x_offset, data.data[1] + self.y_offset), (-1) * self.angle_reset)
        self.odom_path.points[self.odom_path_ctr].x = x 
        self.odom_path.points[self.odom_path_ctr].y = y 
        self.odom_path_ctr = (self.odom_path_ctr + 1) % self.PATH_LEN
        if (self.print_ctr < 3):
            # print ("Constructed ({:.4f}, {:.4f}) from incoming odometry ({:.4f}, {:.4f})".format(x,y,data.data[0],data.data[1]))
            self.print_ctr += 1
        
    def sim_odom_callback(self, data):
        # data_length = data.layout.dim[0].size
        # rospy.loginfo("i heard data %s", data.data)
        
        # add position to path
        self.sim_odom_path.points[self.sim_odom_path_ctr].x = self.x_offset + data.pose.pose.position.x
        self.sim_odom_path.points[self.sim_odom_path_ctr].y = self.y_offset + data.pose.pose.position.y
        self.sim_odom_path_ctr = (self.sim_odom_path_ctr + 1) % self.PATH_LEN
            

    #https://stackoverflow.com/questions/34372480/rotate-point-about-another-point-in-degrees-python#34374437
    def rotate(self, origin, point, angle):
        """
        Rotate a point counterclockwise by a given angle around a given origin.

        The angle should be given in radians.
        """
        ox, oy = origin
        px, py = point

        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        return qx, qy
    

    def __init__(self):
        pos_pub = rospy.Publisher('odom_pos', Marker, queue_size=10)
        path_pub = rospy.Publisher('odom_path', Marker, queue_size=10)
        sim_path_pub = rospy.Publisher('sim_odom_path', Marker, queue_size=10)
        rospy.Subscriber("/amiro1/odom", Odometry, self.sim_odom_callback)
        rospy.Subscriber("odometry", Float32MultiArray, self.odom_callback)
        rospy.Subscriber("pm_status", UInt8MultiArray, self.pm_callback)
        rospy.init_node('display_node', anonymous=True)

        if not rospy.has_param('~coordinate_list'):
            raise Exception('No coordinates found for resetting the map positions.')
        
        
        self.coord_dict = {}
        with open(rospy.get_param('~coordinate_list'), "r") as f:
            data = f.readlines()
            for line in data:
                l = line.rstrip().split(":")
                key_a, key_b = l[0].strip().strip('()').split(', ')
                key_tuple = (int(key_a), int(key_b))
                val_x, val_y, val_alpha = l[1].strip().strip('()').split(', ')
                val_tuple = (float(val_x), float(val_y), float(val_alpha))
                self.coord_dict[key_tuple] = val_tuple
            
        # print(self.coord_dict)
        
        self.x_offset = self.coord_dict[(0,0)][0]
        self.y_offset = self.coord_dict[(0,0)][1]
        self.angle_reset = self.coord_dict[(0,0)][2]
        self.old_pos = 0
        self.print_ctr = 0
        
        self.PATH_LEN = 3000
        
        self.odom_pos = Marker()
        self.odom_pos.header.frame_id = "map"
        self.odom_pos.header.stamp = rospy.get_rostime()
        self.odom_pos.id = 0
        self.odom_pos.type = 3  # cylinder
        self.odom_pos.action = 0
        self.odom_pos.scale.x = 0.05
        self.odom_pos.scale.y = 0.05
        self.odom_pos.scale.z = 0.01
        self.odom_pos.pose.position.x = self.x_offset
        self.odom_pos.pose.position.y = self.y_offset
        self.odom_pos.pose.position.z = 0.5 * self.odom_pos.scale.z
        self.odom_pos.pose.orientation.x = 0
        self.odom_pos.pose.orientation.y = 0
        self.odom_pos.pose.orientation.z = 0
        self.odom_pos.pose.orientation.w = 1.0
        self.odom_pos.color.r = 0.8
        self.odom_pos.color.g = 0.4
        self.odom_pos.color.b = 0.0
        self.odom_pos.color.a = 1.0
        self.odom_pos.lifetime = rospy.Duration(0)

        self.odom_path = Marker()
        self.odom_path.header.frame_id = "map"
        self.odom_path.header.stamp = rospy.get_rostime()
        self.odom_path.id = 2
        self.odom_path.type = 8  # points
        self.odom_path.action = 0
        self.odom_path.scale.x = 0.007
        self.odom_path.scale.y = 0.007
        self.odom_path.scale.z = 0.007
        self.odom_path.pose.position.x = 0.0
        self.odom_path.pose.position.y = 0.0
        self.odom_path.pose.position.z = 0.0
        self.odom_path.pose.orientation.x = 0
        self.odom_path.pose.orientation.y = 0
        self.odom_path.pose.orientation.z = 0
        self.odom_path.pose.orientation.w = 1.0
        self.odom_path.color.r = 0.1
        self.odom_path.color.g = 0.1
        self.odom_path.color.b = 0.7
        self.odom_path.color.a = 1.0
        
        for i in range(self.PATH_LEN):
            self.odom_path.points.append(Point(x=self.x_offset + i, y=self.y_offset, z=0.0))
        self.odom_path_ctr = 0
        self.odom_path.lifetime = rospy.Duration(0)
        
        self.sim_odom_path = Marker()
        self.sim_odom_path.header.frame_id = "map"
        self.sim_odom_path.header.stamp = rospy.get_rostime()
        self.sim_odom_path.id = 2
        self.sim_odom_path.type = 8  # points
        self.sim_odom_path.action = 0
        self.sim_odom_path.scale.x = 0.007
        self.sim_odom_path.scale.y = 0.007
        self.sim_odom_path.scale.z = 0.007
        self.sim_odom_path.pose.position.x = 0.0
        self.sim_odom_path.pose.position.y = 0.0
        self.sim_odom_path.pose.position.z = 0.0
        self.sim_odom_path.pose.orientation.x = 0
        self.sim_odom_path.pose.orientation.y = 0
        self.sim_odom_path.pose.orientation.z = 0
        self.sim_odom_path.pose.orientation.w = 1.0
        self.sim_odom_path.color.r = 0.1
        self.sim_odom_path.color.g = 0.5
        self.sim_odom_path.color.b = 0.1
        self.sim_odom_path.color.a = 1.0
        
        for i in range(self.PATH_LEN):
            self.sim_odom_path.points.append(Point(x=self.x_offset + i, y=self.y_offset, z=0.0))
        self.sim_odom_path_ctr = 0
        self.sim_odom_path.lifetime = rospy.Duration(0)
        
        rate = rospy.Rate(20)
        
        while not rospy.is_shutdown():
            pos_pub.publish(self.odom_pos)
            path_pub.publish(self.odom_path)
            sim_path_pub.publish(self.sim_odom_path)
            rate.sleep()


if __name__ == '__main__':
    try:
        DisplayNode()
    except rospy.ROSInterruptException:
        pass



