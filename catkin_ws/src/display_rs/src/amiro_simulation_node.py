
from time import sleep
import rospy
import math
from std_msgs.msg import UInt8MultiArray, Float32MultiArray, ByteMultiArray, MultiArrayDimension, Duration
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point, Twist, TwistStamped


class AmiroSimulatorNode:

    def __init__(self):
        self.pub = rospy.Publisher('/amiro1/cmd_vel', Twist, queue_size=10)
        rospy.init_node('simulator_node', anonymous=True)
        rate = rospy.Rate(200)
        self.NANOSECONDS_PER_SECOND = 1000000000
        self.MICROSECONDS_PER_SECOND = 1000000
        self.NANOSECONDS_PER_MICROSECOND = 1000
        self.listen_counter = 0
        self.publish_list = []
        self.active = True
        # Wait a bit for the simulation, gazebo and rviz to start
        for i in range(5):
            rospy.loginfo("sleeping for %i second(s)...", 5 - i)
            rospy.sleep(1)
        with open('/home/antigua/Documents/MA/CANROSbridge/catkin_ws/src/display_rs/motor_command_list1.txt', 'r') as f:
            line = f.readline()
            while line != '':
                values = []
                for val in line.split(','):
                    values.append(float(val))
                self.publish_list.append(values)
                line = f.readline()
        self.init_msg = self.publish_list[0]
        rospy.loginfo("Initializing simulation...")
        self.amiro_start_time = self.publish_list[0][0]

        rospy.loginfo("Beginning simulation!")
        self.shout_counter = 0
        cur_time = rospy.Time.now()
        self.sim_start_time = cur_time.secs + (cur_time.nsecs / self.NANOSECONDS_PER_SECOND)
        while not rospy.is_shutdown():
            if len(self.publish_list) > 0:
                cur_time = rospy.Time.now()
                cur_time_accurate = cur_time.secs + (cur_time.nsecs / self.NANOSECONDS_PER_SECOND)
                self.send_dmc_target = Twist()
                motor_command = self.publish_list.pop(0)
                self.send_dmc_target.linear.x = motor_command[1]
                self.send_dmc_target.angular.z = motor_command[2]
                """
                if self.shout_counter < 10:
                    self.shout_counter += 1
                    rospy.loginfo("Relevant Data is\n AMiRo Time: %.2f versus %.2f \n ROS Time: %.2f from %.2f",
                                    amiro_time_accurate, self.amiro_start_time, cur_time_accurate, self.sim_start_time)
                    rospy.loginfo("AMiRo motor command is: %.2f, %.2f at time %.2f",
                                    self.send_dmc_target.twist.linear.x, self.send_dmc_target.twist.angular.z, amiro_time_accurate)
                """
                sleep_time = (motor_command[0] - self.amiro_start_time) - (cur_time_accurate - self.sim_start_time)
                if (sleep_time > 0):
                    # rospy.loginfo("sleeping for %.2f...", sleep_time)
                    rospy.sleep(sleep_time)
                else:
                    # TODO halt simulation and increase time to keep up with execution
                    # rospy.loginfo("Warning! Simulation cannot keep up adequately!")
                    None
                self.pub.publish(self.send_dmc_target)
                """
                sent_time = rospy.Time.now()
                sent_time_accurate = sent_time.secs + (sent_time.nsecs / self.NANOSECONDS_PER_SECOND)
                rospy.loginfo("send motor command: %.2f, %.2f \n AMiRo time was: %.6f \n ROS time was: %.6f \n ROS time at send was: %.6f",
                                    self.send_dmc_target.twist.linear.x, self.send_dmc_target.twist.angular.z,
                                    amiro_time_accurate - self.amiro_start_time,
                                    cur_time_accurate - self.sim_start_time,
                                    sent_time_accurate - self.sim_start_time)
                """
            elif self.active:
                # stop the simulated amiro
                self.active = False
                self.send_dmc_target = Twist()
                self.send_dmc_target.linear.x = 0.0
                self.send_dmc_target.angular.z  = 0.0
                self.pub.publish(self.send_dmc_target)
                rospy.loginfo("Simulated all available data")
            rate.sleep()
            


if __name__ == '__main__':
    try:
        AmiroSimulatorNode()
    except rospy.ROSInterruptException:
        pass



