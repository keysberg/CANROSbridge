#!/usr/bin/env python

from cmath import isclose
from itertools import cycle
import rospy
import math
from std_msgs.msg import Float32MultiArray, UInt8MultiArray, ColorRGBA
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point, Quaternion
from nav_msgs.msg import Odometry
from gazebo_msgs.msg import ModelStates

class RobotDisplayNode:

    def pos_callback(self, data):
        #rospy.loginfo("i heard data %s", data.pose)
        if not math.isclose(data.pose.pose.position.x, self.sim_real_path.points[self.sim_real_path_ctr-1].x, abs_tol=0.00001) and \
            not math.isclose(data.pose.pose.position.y, self.sim_real_path.points[self.sim_real_path_ctr-1].y, abs_tol=0.00001):
            self.sim_real_path.points[self.sim_real_path_ctr].x = data.pose.pose.position.x
            self.sim_real_path.points[self.sim_real_path_ctr].y = data.pose.pose.position.y
            self.sim_real_path_ctr = (self.sim_real_path_ctr + 1) % self.PATH_LEN
            rospy.loginfo("Latest simulation position no. %i at:(%.5f,%.5f)", self.sim_real_path_ctr, data.pose.pose.position.x, data.pose.pose.position.y)

    def odom_callback(self, data):
        # data_length = data.layout.dim[0].size
        # rospy.loginfo("i heard data %s", data.data)
        
        # add position to path
        self.odom_path.points[self.odom_path_ctr].x = data.data[0]
        self.odom_path.points[self.odom_path_ctr].y = data.data[1]
        self.odom_path_ctr = (self.odom_path_ctr + 1) % self.PATH_LEN
        rospy.loginfo("Latest odom position no. %i at:(%.4f,%.4f)", self.odom_path_ctr, data.data[0], data.data[1])
            
    def ypr_to_quaternion(self, yaw, pitch, roll):
        """
        Transforms (yaw, pitch, roll)-rotations to quaternion orientation.
        """
        cy = math.cos(yaw * 0.5)
        sy = math.sin(yaw * 0.5)
        cp = math.cos(pitch * 0.5)
        sp = math.sin(pitch * 0.5)
        cr = math.cos(roll * 0.5)
        sr = math.sin(roll * 0.5)

        q = Quaternion()
        q.w = cr * cp * cy + sr * sp * sy
        q.x = sr * cp * cy - cr * sp * sy
        q.y = cr * sp * cy + sr * cp * sy
        q.z = cr * cp * sy - sr * sp * cy
        return q

    """
    int32 id                                    # object ID useful in conjunction with the namespace for manipulating and deleting the object later
    int32 type                                  # Type of object (3: Cylinder, 8: Points)
    int32 scale                                 # size in x,y,0.5*z
    geometry_msgs/Point position                # position x,y,z
    geometry_msgs/Quaternion orientation        # orientation in x,y,z,w quaternion
    std_msgs/ColorRGBA color                    # Color [0.0-1.0]
    """
    def init_marker(self, id, type, scale, position, orientation, color):
        new_marker = Marker()
        new_marker.header.frame_id = "map"
        new_marker.header.stamp = rospy.get_rostime()
        new_marker.id = id
        new_marker.type = type
        new_marker.action = 0
        new_marker.scale.x = scale
        new_marker.scale.y = scale
        new_marker.scale.z = 0.5 * scale
        new_marker.pose.position = position
        new_marker.pose.orientation = orientation
        new_marker.color = color
        new_marker.lifetime = rospy.Duration(0)
        return new_marker
        

    def __init__(self):
        #path_pub = rospy.Publisher('odom_path', Marker, queue_size=10)
        sim_real_path_pub = rospy.Publisher('sim_real_path', Marker, queue_size=10)
        #rospy.Subscriber("/odometry", Float32MultiArray, self.odom_callback)
        rospy.Subscriber("/amiro1/odom", Odometry, self.pos_callback)
        rospy.init_node('display_node', anonymous=True)
    
        self.PATH_LEN = 10000
        
        # setup objects for rviz
        self.x_offset, self.y_offset, self.rotation = (0.0, 0.0, 0.0)
        if rospy.has_param('~x_pos'):
            self.x_offset = rospy.get_param('~x_pos')
        if rospy.has_param('~y_pos'):
            self.y_offset = rospy.get_param('~y_pos')
        if rospy.has_param('~rotation'):
            self.rotation = rospy.get_param('~rotation')
        start_position = Point(x=self.x_offset, y=self.y_offset, z=0.0)
        start_orientation = self.ypr_to_quaternion(self.rotation, 0.0, 0.0)
        # visualize the AMiRo internal odometry as blue path
        #self.odom_path = self.init_marker(0, 8, 0.007, start_position, start_orientation, ColorRGBA(r=0.1, g=0.1, b=0.7, a=1.0))
        # visualize the ground truth position of the Gazebo Robot as green path
        self.sim_real_path = self.init_marker(1, 8, 0.007, start_position, start_orientation, ColorRGBA(r=0.1, g=0.5, b=0.1, a=1.0))
        
        # append a number of actual points
        for i in range(self.PATH_LEN):
            #self.odom_path.points.append(Point(x=self.x_offset + i, y=self.y_offset, z=0.0))
            self.sim_real_path.points.append(Point(x=self.x_offset - i, y=self.y_offset, z=0.0))
        #self.odom_path_ctr = 0
        self.sim_real_path_ctr = 0
        
        rate = rospy.Rate(30)
        
        while not rospy.is_shutdown():
            #path_pub.publish(self.odom_path)
            sim_real_path_pub.publish(self.sim_real_path)
            rate.sleep()


if __name__ == '__main__':
    try:
        RobotDisplayNode()
    except rospy.ROSInterruptException:
        pass



