#!/usr/bin/env python

from itertools import cycle
import rospy
import math
from std_msgs.msg import Float32MultiArray, UInt8MultiArray, ColorRGBA
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point, Quaternion
from nav_msgs.msg import Odometry
from gazebo_msgs.msg import ModelStates

class DisplayNode:

    def pm_callback(self, data):
        # data_length = data.layout.dim[0].size
        #rospy.loginfo("i heard data %s", data.data)
        """
        if (data.data[0] == 0):
            self.old_pos = new_pos = 0
            print("Reset position due to standby.")
        else:
            new_pos = data.data[6]
        if (self.old_pos,new_pos) in self.coord_dict:
            self.odom_pos.pose.position.x, self.odom_pos.pose.position.y, angle = self.coord_dict[(self.old_pos, new_pos)] 
            print("Arrived at position ({}-{}) with original coordinates (X:{},Y:{})".format(
                self.old_pos,new_pos, self.odom_pos.pose.position.x, self.odom_pos.pose.position.y))
        else:
            print("Key ({}-{}) not found :O".format(self.old_pos, new_pos))
        
        self.old_pos = new_pos
        # show the latest secured position on map
        self.odom_pos.scale.z = 0.01
        self.odom_pos.pose.position.z = 0.5 * self.odom_pos.scale.z
        """
        None
        
            
    def odom_callback(self, data):
        # data_length = data.layout.dim[0].size
        # rospy.loginfo("i heard data %s", data.data)
        
        # add position to path
        self.odom_path.points[self.odom_path_ctr].x = data.data[0]
        self.odom_path.points[self.odom_path_ctr].y = data.data[1]
        self.odom_path_ctr = (self.odom_path_ctr + 1) % self.PATH_LEN
        
    def sim_odom_callback(self, data):
        # data_length = data.layout.dim[0].size
        # rospy.loginfo("i heard data %f, %f", data.pose.pose.position.x, data.pose.pose.position.y)
        
        # add position to path
        self.sim_odom_path.points[self.sim_odom_path_ctr].x = data.pose.pose.position.x
        self.sim_odom_path.points[self.sim_odom_path_ctr].y = data.pose.pose.position.y
        self.sim_odom_path_ctr = (self.sim_odom_path_ctr + 1) % self.PATH_LEN
            
    def ypr_to_quaternion(self, yaw, pitch, roll):
        """
        Transforms (yaw, pitch, roll)-rotations to quaternion orientation.
        """
        cy = math.cos(yaw * 0.5)
        sy = math.sin(yaw * 0.5)
        cp = math.cos(pitch * 0.5)
        sp = math.sin(pitch * 0.5)
        cr = math.cos(roll * 0.5)
        sr = math.sin(roll * 0.5)

        q = Quaternion()
        q.w = cr * cp * cy + sr * sp * sy
        q.x = sr * cp * cy - cr * sp * sy
        q.y = cr * sp * cy + sr * cp * sy
        q.z = cr * cp * sy - sr * sp * cy
        return q

    """
    int32 id                                    # object ID useful in conjunction with the namespace for manipulating and deleting the object later
    int32 type                                  # Type of object (3: Cylinder, 8: Points)
    int32 scale                                 # size in x,y,0.5*z
    geometry_msgs/Point position                # position x,y,z
    geometry_msgs/Quaternion orientation        # orientation in x,y,z,w quaternion
    std_msgs/ColorRGBA color                    # Color [0.0-1.0]
    """
    def init_marker(self, id, type, scale, position, orientation, color):
        new_marker = Marker()
        new_marker.header.frame_id = "map"
        new_marker.header.stamp = rospy.get_rostime()
        new_marker.id = id
        new_marker.type = type
        new_marker.action = 0
        new_marker.scale.x = scale
        new_marker.scale.y = scale
        new_marker.scale.z = 0.5 * scale
        new_marker.pose.position = position
        new_marker.pose.orientation = orientation
        new_marker.color = color
        new_marker.lifetime = rospy.Duration(0)
        return new_marker
        

    def __init__(self):
        pos_pub = rospy.Publisher('odom_pos', Marker, queue_size=10)
        path_pub = rospy.Publisher('odom_path', Marker, queue_size=10)
        sim_path_pub = rospy.Publisher('sim_odom_path', Marker, queue_size=10)
        rospy.Subscriber("/amiro1/odom", Odometry, self.sim_odom_callback)
        rospy.Subscriber("/odometry", Float32MultiArray, self.odom_callback)
        rospy.Subscriber("pm_status", UInt8MultiArray, self.pm_callback)
        rospy.init_node('display_node', anonymous=True)

        if not rospy.has_param('~coordinate_list'):
            raise Exception('No coordinates found for resetting the map positions.')
        
        
        self.coord_dict = {}
        with open(rospy.get_param('~coordinate_list'), "r") as f:
            data = f.readlines()
            for line in data:
                l = line.rstrip().split(":")
                key_a, key_b = l[0].strip().strip('()').split(', ')
                key_tuple = (int(key_a), int(key_b))
                val_x, val_y, val_alpha = l[1].strip().strip('()').split(', ')
                val_tuple = (float(val_x), float(val_y), float(val_alpha))
                self.coord_dict[key_tuple] = val_tuple
            
        # print(self.coord_dict)
        
        self.x_offset = self.coord_dict[(0,0)][0]
        self.y_offset = self.coord_dict[(0,0)][1]
        self.angle_reset = self.coord_dict[(0,0)][2]
        self.old_pos = 0
        self.print_ctr = 0
        
        self.PATH_LEN = 6000
        
        # setup objects for rviz
        start_position = Point(x=self.x_offset, y=self.y_offset, z=0.0)
        start_orientation = self.ypr_to_quaternion(-self.angle_reset, 0.0, 0.0)
        # visualize the latest secured Position as cylinder
        self.odom_pos = self.init_marker(0, 3, 0.05, start_position, start_orientation, ColorRGBA(r=0.8, g=0.4, b=0.0, a=1.0))
        # visualize the AMiRo internal odometry as blue path
        self.odom_path = self.init_marker(1, 8, 0.007, start_position, start_orientation, ColorRGBA(r=0.1, g=0.1, b=0.7, a=1.0))
        # visualize the Gazebo Robot's odometry as green path
        self.sim_odom_path = self.init_marker(2, 8, 0.007, start_position, start_orientation, ColorRGBA(r=0.1, g=0.5, b=0.1, a=1.0))
        
        # append a number of actual points
        for i in range(self.PATH_LEN):
            self.odom_path.points.append(Point(x=self.x_offset + i, y=self.y_offset, z=0.0))
            self.sim_odom_path.points.append(Point(x=self.x_offset + i, y=self.y_offset, z=0.0))
        self.odom_path_ctr = 0
        self.sim_odom_path_ctr = 0
        
        rate = rospy.Rate(30)
        
        while not rospy.is_shutdown():
            pos_pub.publish(self.odom_pos)
            path_pub.publish(self.odom_path)
            sim_path_pub.publish(self.sim_odom_path)
            rate.sleep()


if __name__ == '__main__':
    try:
        DisplayNode()
    except rospy.ROSInterruptException:
        pass



